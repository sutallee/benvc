GNU_BIN=$(GNU_ROOT)/bin

############################################################
# Library archiver
#
AR=$(GNU_BIN)/m68k-elf-ar

############################################################
# Assembler
#
AS=$(GNU_BIN)/m68k-elf-as

############################################################
# C compiler
#
CC=$(GNU_BIN)/m68k-elf-gcc

############################################################
# C++ compiler
#
CXX=$(GNU_BIN)/m68k-elf-gcc

############################################################
# Object file inspector
#
NM=$(GNU_BIN)/m68k-elf-nm

############################################################
# C pre-processor
#
CPP=$(CC) -E

############################################################
# Library Archive flags
#
ARFLAGS=rv

############################################################
# Assembler flags
#
ASFLAGS= $(INC)

############################################################
# Flags which may optionally be overriden by the user
#

OPTIMIZATION_CFLAG=-O2
DEBUG_CFLAG=-g

############################################################
# C compiler flags
#

CFLAGS= $(INC) \
	-mnobitfield -msoft-float \
    -fno-exceptions \
    -Wall \
    $(OPTIMIZATION_CFLAG) \
    $(DEBUG_CFLAG)

############################################################
# C++ compiler flags
#
CXXFLAGS=-pipe -fno-rtti 

############################################################
# RCS check-out flags
#
COFLAGS=

############################################################
# C pre-processor flags
#
CPPFLAGS=

############################################################
# Linker flags
#
LDFLAGS=
FINAL_LDFLAGS=-Map=$(FINAL_OUTPUT_FILENAME).map

############################################################
# Linker libraries
#
#TOOLCHAINLIBS=-lg -lstdc++ -liberty
TOOLCHAINLIBS=

############################################################
# Include paths
#
INC= -I. \
	-I$(buildroot) \
	$(libincs) \
	$(LOCALINC) \
	$(LIB_INC_ROOTS) \
	$(OS_INC_ROOTS)

############################################################
# Linker and variants
#

LD=$(GNU_BIN)/m68k-elf-ld
INCREMENTAL_LINK=$(LD) -r $(LDFLAGS)
FINAL_LINK=$(LD) $(FINAL_LDFLAGS)

############################################################
# Object file dumper
#

OBJDUMP=$(GNU_BIN)/m68k-elf-objdump

############################################################
# Object file copier
#

OBJCOPY=$(GNU_BIN)/m68k-elf-objcopy

############################################################
# Dependency makefile fragment generation
#
CPP_DEP_GEN= \
	$(SHELL) -ec '$(CXX) -M $(benvFlagsCpp) \
	$< | $(addTargetFilter)' > $@

C_DEP_GEN= \
	$(SHELL) -ec '$(CC) -M $(benvFlagsC) \
	$< | $(addTargetFilter)' > $@

ASM_DEP_GEN= \
	$(SHELL) -ec '$(CC) -M $(benvAsmDepFlags) \
	$< | $(addTargetFilter)' > $@

MRI_ASM_DEP_GEN= \
	$(SHELL) -ec '$(CC) -M $(benvAsmDepFlags) \
	$< | $(adjustAsmObj) | $(addTargetFilter)' > $@

