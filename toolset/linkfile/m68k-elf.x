/*
 * This script is a generic C++ linker script for embedded targets
 * which require that the initialized data segment be loaded into
 * the text segment for PROMS.
 */

STARTUP(src/oscl/ooos/linker/m68k/crt0.o)
/* OUTPUT_FORMAT("elf-m68k") */
/* SEARCH_DIR(/tools/gnu/lib/gcc-lib/m68k-elf/2.95); */

SECTIONS
{
  .text : {
    *(.text)
    *(.rodata.str1.1)
    *(.gcc_exc)
    *(.eh_fram)
    *(.eh_frame)
	*(.gnu.linkonce*)
  } > rom

  .cdtors : {
	. = ALIGN(0x4);
     __CTOR_LIST__ = .;
     __CTOR_LIST__ = .;
     LONG((__CTOR_END__ - __CTOR_LIST__) / 4 - 2)
     *(.ctors)
     LONG(0)
     __CTOR_END__ = .;
     __DTOR_LIST__ = .;
     LONG((__DTOR_END__ - __DTOR_LIST__) / 4 - 2)
     *(.dtors)
     LONG(0)
     __DTOR_END__ = .;

/** Start New stuff **/
    *(.rodata)
    *(.gcc_except_table) 

    __INIT_SECTION__ = . ;
    LONG (0x4e560000)	/* linkw %fp,#0 */
    *(.init)
    SHORT (0x4e5e)	/* unlk %fp */
    SHORT (0x4e75)	/* rts */

    __FINI_SECTION__ = . ;
    LONG (0x4e560000)	/* linkw %fp,#0 */
    *(.fini)
    SHORT (0x4e5e)	/* unlk %fp */
    SHORT (0x4e75)	/* rts */
/** End New stuff **/

     etext  =  .;
	} > rom

  .data : AT (ADDR(.cdtors) + SIZEOF(.cdtors)){
	_data	= .;
    *(.data)
     edata  =  .;
  } > ram

  .bss : {
     __bss_start = .;
    *(.bss)
    *(COMMON)
      end = ALIGN(0x8);
      _end = ALIGN(0x8);
     __bss_end = .;
  } > ram

  .stab  0 (NOLOAD) : 
  {

    [ .stab ]
  }

  .stabstr  0 (NOLOAD) :
  {
    [ .stabstr ]
  }
}
