#!/bin/bash

# This script takes a number representing the
# number of seconds since the unix epoch (1970-01-01 00:00:00 UTC)
# and returns the date/time for that number.

if [ -z $1 ]; then
	echo "timestamp required as an argument"
	exit 1;
fi

timestamp=$1


date --date=@$timestamp
