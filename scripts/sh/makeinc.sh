#!/bin/bash

# The purpose of this script is to provide a list
# of all included library files (e.g. libdirs.b).

topfile=$1
topdir=$2
file="$1"

function expandInc() {
	local topfile=$1
	local includes=`grep '^+' ${topfile} | sed s%^+%%`
	local libs=`grep '^[^+#]' ${topfile}`

	for f in ${includes}; do
		expandInc ${topdir}/${f} ${topdir}
	done

	# Depth first such that the
	# top-most directory is listed last.
	echo "${libs}"
	}

expandInc ${topfile} ${topdir}

