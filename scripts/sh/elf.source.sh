#!/bin/bash
#
# This script is intended to generate a list of
# source files actually used by a particular BENVC project
# to stdout.
#
# Usage: elf.source.sh [-a] [-t] <elf-binary>
#
#	-a	The output uses absolute path names. By default
#		path-names are relative to the BENV root directory.
#
#	-t	The output contains toolchain header files
#
# The source files are obtained by:
# 1.	using the standard readelf utility to obtain
#		a list of source files actually used from the
#		project's elf binary.
# 2.	The list is then munged to obtain the corresponding
#		relative derived object directory paths.
# 3.	The BENVC generated depenedency files at thes paths
#		are read and a list of all header and source files
#		are obtained.
# 4.	The list is sorted and duplicates are removed.
# 5.	Unless the second argument is present, toolchain
#		provided files are removed from the list.
#


while getopts ":ta" opt; do
	case ${opt} in
		a ) # process option h
			absolute="yes"
			;;
		t ) # process option t
			toolchain="yes"
			;;
		\? )
			echo "Usage: cmd [-h] [-t]"
			;;
		: )
			 echo "Invalid option: $OPTARG requires an argument" 1>&2
			;;
	esac
done

shift $((OPTIND-1))

projpath=$PWD

# The BENV root directory for this project.
rootpath=${projpath%%project*}

# The project directory relative to the BENV root directory.
projpath=${projpath#$rootpath}

# Create a list of all of the source files actually linked into the application.
# This list will NOT contain source files that were compiled and not used.
sources=`readelf --string-dump=.debug_str $1 | grep -e '\.cpp$' -e '\.c$' -e '\.s$' -e '\.S$' | sed 's/^ *\[.*\] *//'`

source=""
syssource=""

list=""
# Copy the content of all dependency files "*.d"
# into a single temporary file.
for line in $sources; do
	basename=${line%.*}
	basename=${basename##$rootpath}
	tmp=`cat $basename.d`
	list="${list}"$'\n'"${tmp}"
done

# Create files list by extracting one instance of each prerequisite
# from
# sed 's/.*://'			- remove make targets
# sed 's/ *\\$//'		- remove line continuations
# sed 's/ /\n/'			- replace spaces with newlines
# grep '[a-zA-Z0-9]'	- remove blank lines

files=$(echo "$list" | sed 's/.*://' | sed 's/ *\\$//' | sed 's/ /\n/' | grep '[a-zA-Z0-9]' | sort | uniq)
for f in $files; do
	# Remove prefix for our source code
	mf=${f#$rootpath}
	if [[ "$mf" != "$f" ]]; then
		# A list of our source code
		if [[ -n "$absolute" ]]; then
			source="$source $rootpath$mf"
		else
			source="$source $mf"
		fi
	else
		# if we get here, the file is either
		# a toolchain/system file or a
		# file located in the project directory.
		# if the file name begins with a '/', then
		# it is a toolchain/system file, otherwise
		# it is a project file.
		sysfile=${mf#/}
		if [[ "$sysfile" == "$mf" ]]; then
			# we have a project file
			if [[ -n "$absolute" ]]; then
				source="$source $PWD/$mf"
			else
				source="$source $projpath/$mf"
			fi
		else
			# we have a toolchain/system file
			syssource="$syssource $mf"
		fi
	fi
done

for s in $source;do
	echo "$s"
done

if [[ -n "$toolchain" ]]; then
for s in $syssource;do
	echo "$s"
done
fi

